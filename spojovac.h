#ifndef SPOJOVAC_H
#define SPOJOVAC_H

#include "balicek.h"

#define STREAM_IGNORE 0
#define STREAM_HLAVICKA 1
#define STREAM_DATA 2


class Spojovac
{
public:
    Spojovac();
    ~Spojovac();

    virtual void odesliHlavicku(struct rsaBalik_L2 &hlavicka, bool mame_naklad) {};
    virtual void odesliNaklad(balikNaklad_t &naklad) {};

    virtual int prijmi(struct rsaBalik_L2 &hlavicka, balikNaklad_t &naklad) {return -1;};
};


#endif // SPOJOVAC_H
