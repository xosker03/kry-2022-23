
NUM_CORES = $(shell nproc)

CXX = g++

CFLAGS = -Ofast -DNODEBUG
CPPFLAGS = -std=c++2a -fpermissive
GCCFLAGS = -march=native -mtune=native

DEPENDENCIES = main.o zaklad.o saes.o rsus.o podpora.o balik.o spojovac.o klient.o server.o

COMMAND = -o kry $(DEPENDENCIES)

LDFLAGS = -lpthread -lcryptopp -lsctp


all: $(DEPENDENCIES)
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)

build: $(DEPENDENCIES)
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)

run:
	./kry -${TYPE} -p ${PORT}

clean:
	rm -f kry
	rm -f *.o

#all:
#	g++ -g -fpermissive -o kry main.cpp zaklad.cpp saes.cpp rsus.cpp podpora.cpp balik.cpp spojovac.cpp -lpthread -lcryptopp

#build:
#	g++ -std=c++2a -march=native -mtune=native -Ofast -o kry main.cpp zaklad.cpp saes.cpp rsus.cpp podpora.cpp balik.cpp spojovac.cpp -lpthread -lcryptopp

zip:
	zip -r 204496.zip Makefile *.cpp *.h *.hpp dokumentace.pdf klice


%.o: %.cpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) -o $@ -c $<


#-Wno-writable-strings
#-fopenmp
