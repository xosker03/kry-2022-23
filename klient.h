#ifndef KLIENT_H
#define KLIENT_H

#include "spojovac.h"

using namespace std;




class Klient : public Spojovac
{
public:
    Klient(int port, string ip = "127.0.0.1");
    ~Klient();

    void odesliHlavicku(struct rsaBalik_L2 &hlavicka, bool mame_naklad) override;
    void odesliNaklad(balikNaklad_t &naklad) override;

    int prijmi(struct rsaBalik_L2 &hlavicka, balikNaklad_t &naklad) override;

private:
    int conn_fd;
};

#endif // KLIENT_H
