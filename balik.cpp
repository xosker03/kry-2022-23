#include "balik.h"
#include "zaklad.hpp"
#include "podpora.h"

struct rsaBalik_L0 {
    union {
        struct rsaBalik_L1 L1;
        struct {
            union {
                uint8_t kolotoc1[8];
                uint16_t kolotoc2[4];
                uint32_t kolotoc3[2];
                uint64_t kolotoc4[1];
            };
            union {
                uint8_t kolo1[KOLOSIZE_1];
                uint16_t kolo2[KOLOSIZE_2];
                uint32_t kolo3[KOLOSIZE_3];
                uint64_t kolo4[KOLOSIZE_4];
            };
        };
    };
};


static_assert(sizeof(enum typZpravy) == SIZE_typZpravy, "CHYBA: nesedí velikost enum typZpravy");
static_assert(sizeof(struct ridic) == SIZE_ridic, "CHYBA: nesedí velikost struct ridic");
static_assert(sizeof(struct rsaBalik_L0) == SIZE_rsaBalik_L0, "CHYBA: nesedí velikost struct rsaBalik_L0");
static_assert(sizeof(struct rsaBalik_L1) == SIZE_rsaBalik_L1, "CHYBA: nesedí velikost struct rsaBalik_L1");
static_assert(sizeof(struct rsaBalik_L2) == SIZE_rsaBalik_L2, "CHYBA: nesedí velikost struct rsaBalik_L2");



#define PAes(PREDTEXT)\
    cout << bc.YELLOW << SERVER << PREDTEXT << "AES_key=" << bc.CYAN;\
    for(int i = 0; i < AES_KLIC_VEL; ++i){\
        printf("%.2x", balicek.hlavicka.aes_klic[i]);\
    }\
    cout << bc.ENDC << endl;\
    cout << bc.YELLOW << SERVER << PREDTEXT << "AES_iv=" << bc.DCYAN;\
    for(int i = 0; i < AES_IV_VEL; ++i){\
        printf("%.2x", balicek.hlavicka.aes_iv[i]);\
    }\
    cout << bc.ENDC << endl;


#define PAesPad(_JMENO)\
    cout << bc.YELLOW << SERVER << _JMENO << bc.BLUE;\
    for(int i = 0 ; i < SIZE_rsaBalik_L1; ++i){\
        if(i == 8)\
            cout << bc.RED;\
        if(i == sizeof(balicek.hlavicka.ridic) + 8)\
            cout << bc.CYAN;\
        if(i == sizeof(balicek.hlavicka.ridic) + 8 + AES_KLIC_VEL)\
            cout << bc.DCYAN;\
        if(i == sizeof(balicek.hlavicka.ridic) + 8 + AES_KLIC_VEL + AES_IV_VEL)\
            cout << bc.BLUE;\
        printf("%.2x", ((uint8_t *)&balicek)[i]);\
    }\
    cout << bc.ENDC << endl;


#define PMD5(_JMENO, _L2L1)\
    cout << bc.YELLOW << SERVER << _JMENO << bc.CYAN;\
    for(int i = 0; i < HASH_SIZE;++i){\
        printf("%.2x", balicek._L2L1.podpis[i]);\
    }\
    cout << bc.ENDC << endl;\


#define PMD5Pad(_JMENO, _L2L1)\
    cout << bc.YELLOW << SERVER << _JMENO << bc.BLUE;\
    for(int i = 0 ; i < SIZE_rsaBalik_L1; ++i){\
        if(i == 8)\
            cout << bc.RED;\
        if(i == sizeof(balicek._L2L1.ridic) + 8)\
            cout << bc.CYAN;\
        if(i == sizeof(balicek._L2L1.ridic) + 8 + HASH_SIZE)\
            cout << bc.BLUE;\
        printf("%.2x", ((uint8_t *)&balicek._L2L1)[i]);\
    }\
    cout << bc.ENDC << endl;







Balik::Balik(RSUS *_mojeRsa, RSUS *_ciziRsa, Spojovac *_spojka)
{
    memset(&balicek, 0, sizeof(balicek));
    mojeRsa = _mojeRsa;
    ciziRsa = _ciziRsa;
    spojka = _spojka;
}

Balik::Balik(const Balik& other){
    mojeRsa = other.mojeRsa;
    ciziRsa = other.ciziRsa;
    spojka = other.spojka;
}

Balik::~Balik()
{

}

void Balik::vlozAck(bool ack){
    memset(&balicek, 0, sizeof(balicek));
    gimmeRandoms(&balicek.hlavicka.kolotoc ,8);
    *((uint8_t*)&balicek.hlavicka.kolotoc) = 0;

    naklad3 = balikNaklad_t();

    if(ack)
        balicek.hlavicka.ridic.typ = ackDat_e;
    else
        balicek.hlavicka.ridic.typ = nackDat_e;


    balicek.podpis_hlavicky.ridic.typ = podpisHlavicky_e;
    balicek.podpis_dat.ridic.typ = nepouzito_e;
    gimmeRandoms(&balicek.hlavicka.naklad, MAX_PAYLOAD);

    klic = mujAes.aesGenKlic();
    iv = mujAes.aesGenIV();
    mujAes.aesSet(klic, iv);

    for(int i = 0; i < AES_KLIC_VEL; ++i){
        balicek.hlavicka.aes_klic[i] = klic[i];
    }
    for(int i = 0; i < AES_IV_VEL; ++i){
        balicek.hlavicka.aes_iv[i] = iv[i];
    }
    mame_naklad = false;

    PAes("");
    PAesPad("AES_Padding=");
}

void Balik::vlozNaklad(balikNaklad_t data){
    memset(&balicek, 0, sizeof(balicek));
    gimmeRandoms(&balicek.hlavicka.kolotoc ,8);
    *((uint8_t*)&balicek.hlavicka.kolotoc) = 0;

    naklad3 = data;
    balicek.hlavicka.ridic.typ = poslaniDat_e;
    balicek.hlavicka.ridic.velikost_aes_nakladu = data.size();

    balicek.podpis_hlavicky.ridic.typ = podpisHlavicky_e;
    balicek.podpis_dat.ridic.typ = podpisDat_e;


    klic = mujAes.aesGenKlic();
    iv = mujAes.aesGenIV();
    mujAes.aesSet(klic, iv);

    gimmeRandoms(&balicek.hlavicka.aes_naklad, MAX_PAYLOAD - AES_IV_VEL - AES_KLIC_VEL);

    for(int i = 0; i < AES_KLIC_VEL; ++i){
        balicek.hlavicka.aes_klic[i] = klic[i];
    }
    for(int i = 0; i < AES_IV_VEL; ++i){
        balicek.hlavicka.aes_iv[i] = iv[i];
    }
    mame_naklad = true;

    PAes("");
    PAesPad("AES_Padding=");
}


void Balik::podepisHlavicku(){
    md5hash_t hashHlavicky;

    hashHlavicky = md5Hash(&balicek.hlavicka, sizeof(balicek.hlavicka));

    for(int i = 0; i < HASH_SIZE;++i){
        balicek.podpis_hlavicky.podpis[i] = hashHlavicky[i];
    }
    gimmeRandoms(&balicek.podpis_hlavicky.podpisova_vypln, MAX_PAYLOAD - HASH_SIZE);
    gimmeRandoms(&balicek.podpis_hlavicky.kolotoc ,8);
    *((uint8_t*)&balicek.podpis_hlavicky.kolotoc) = 0;


    PMD5("MD5_aes-key=", podpis_hlavicky);
    PMD5Pad("MD5_aes-key_padding=", podpis_hlavicky);
    xolotoc(&balicek.podpis_hlavicky);

    mojeRsa->rsaPodepisuj(&balicek.podpis_hlavicky, sizeof(balicek.podpis_hlavicky));
}

bool Balik::overPodpisHlavcky(){
    ciziRsa->rsaOveruj(&balicek.podpis_hlavicky, sizeof(balicek.podpis_hlavicky));
    colotox(&balicek.podpis_hlavicky);

    md5hash_t hashHlavicky;
    md5hash_t hashPodpisu;

    hashHlavicky = md5Hash(&balicek.hlavicka, sizeof(balicek.hlavicka));

    for(int i = 0; i < HASH_SIZE; ++i)
        hashPodpisu.push_back(balicek.podpis_hlavicky.podpis[i]);


    PMD5("Recieved_MD5_aes-key=", podpis_hlavicky);
    cout << bc.YELLOW << SERVER << "Recieved_aes-key_MD5=" << bc.CYAN;
    for(int i = 0; i < HASH_SIZE;++i){
        printf("%.2x", hashHlavicky[i]);
    }
    cout << bc.ENDC << endl;
    return hashHlavicky == hashPodpisu;
}



void Balik::podepisNaklad(){
    md5hash_t hashDat;

    hashDat = md5Hash((void*) naklad3.c_str(), naklad3.size());

    int idx = 0;
    for(uint8_t a : hashDat)
        balicek.podpis_dat.podpis[idx++] = a;
    gimmeRandoms(&balicek.podpis_dat.podpisova_vypln, MAX_PAYLOAD - HASH_SIZE);
    gimmeRandoms(&balicek.podpis_dat.kolotoc ,8);
    *((uint8_t*)&balicek.podpis_dat.kolotoc) = 0;


    PMD5("MD5_message=", podpis_dat);
    PMD5Pad("MD5_message_padding=", podpis_dat);
    xolotoc(&balicek.podpis_dat);

    mojeRsa->rsaPodepisuj(&balicek.podpis_dat, sizeof(balicek.podpis_dat));
}

bool Balik::overPodpisNakladu(){
    ciziRsa->rsaOveruj(&balicek.podpis_dat, sizeof(balicek.podpis_dat));
    colotox(&balicek.podpis_dat);

    md5hash_t hashDat;
    md5hash_t hashPodpisu;

    hashDat = md5Hash((void*) naklad3.c_str(), naklad3.size());

    for(int i = 0; i < HASH_SIZE; ++i)
        hashPodpisu.push_back(balicek.podpis_dat.podpis[i]);

    PMD5("Recieved_MD5_message=", podpis_dat);
    cout << bc.YELLOW << SERVER << "Recieved_message_MD5=" << bc.CYAN;
    for(int i = 0; i < HASH_SIZE;++i){
        printf("%.2x", hashDat[i]);
    }
    cout << bc.ENDC << endl;
    return hashDat == hashPodpisu;
}



void Balik::zasifrujHlavicku(){
    xolotoc(&balicek.hlavicka);
    ciziRsa->rsaSifruj(&balicek.hlavicka, sizeof(balicek.hlavicka));
    //ciziRsa->rsaSifruj(&balicek.podpis_hlavicky, sizeof(balicek.podpis_hlavicky));

    vector<u_int8_t> temp_podpis;
    temp_podpis.resize(SIZE_rsaBalik_L1);
    *((struct rsaBalik_L1 *)temp_podpis.data()) = balicek.podpis_hlavicky;
    vector<u_int8_t> temp_zasifrovany_podpis = mujAes.aesSifruj(temp_podpis);
    balicek.podpis_hlavicky = *((struct rsaBalik_L1 *)temp_zasifrovany_podpis.data());

    PAesPad("RSA_AES_Padding=");
    PMD5Pad("AES_MD5_aes-key_padding=", podpis_hlavicky);
}

void Balik::desifrujHlavicku(){
    mojeRsa->rsaDesifruj(&balicek.hlavicka, sizeof(balicek.hlavicka));
    //mojeRsa->rsaDesifruj(&balicek.podpis_hlavicky, sizeof(balicek.podpis_hlavicky));
    colotox(&balicek.hlavicka);

    klic = aesKlic_t();
    iv = aesIV_t();
    for(int i = 0; i < AES_KLIC_VEL; ++i){
        klic.push_back(balicek.hlavicka.aes_klic[i]);
    }
    for(int i = 0; i < AES_IV_VEL; ++i){
        iv.push_back(balicek.hlavicka.aes_iv[i]);
    }
    mujAes.aesSet(klic, iv);

    vector<u_int8_t> temp_zasifrovany_podpis;
    temp_zasifrovany_podpis.resize(SIZE_rsaBalik_L1);
    *((struct rsaBalik_L1 *)temp_zasifrovany_podpis.data()) = balicek.podpis_hlavicky;
    vector<u_int8_t> temp_podpis = mujAes.aesDesifruj(temp_zasifrovany_podpis);
    balicek.podpis_hlavicky = *((struct rsaBalik_L1 *)temp_podpis.data());
}



void Balik::zasifrujNaklad(){

    //mujAes.aesSet(klic, iv);

    vector<u_int8_t> temp_podpis;
    temp_podpis.resize(SIZE_rsaBalik_L1);
    *((struct rsaBalik_L1 *)temp_podpis.data()) = balicek.podpis_dat;
    vector<u_int8_t> temp_zasifrovany_podpis = mujAes.aesSifruj(temp_podpis);
    balicek.podpis_dat = *((struct rsaBalik_L1 *)temp_zasifrovany_podpis.data());

    naklad3 = mujAes.aesSifruj(naklad3);

    PMD5Pad("AES_MD5_message_padding=", podpis_dat);

    //ciziRsa->rsaSifruj(&balicek.podpis_dat, sizeof(balicek.podpis_dat));
    cout << bc.YELLOW << SERVER << "ciphertext=" << bc.ENDC;
    for(auto a : naklad3){
        printf("%.2x", a);
    }
    cout << endl;
}

void Balik::desifrujNaklad(){

    //mujAes.aesSet(klic, iv);

    vector<u_int8_t> temp_zasifrovany_podpis;
    temp_zasifrovany_podpis.resize(SIZE_rsaBalik_L1);
    *((struct rsaBalik_L1 *)temp_zasifrovany_podpis.data()) = balicek.podpis_dat;
    vector<u_int8_t> temp_podpis = mujAes.aesDesifruj(temp_zasifrovany_podpis);
    balicek.podpis_dat = *((struct rsaBalik_L1 *)temp_podpis.data());


    naklad3 = mujAes.aesDesifruj(naklad3);

    //mojeRsa->rsaDesifruj(&balicek.podpis_dat, sizeof(balicek.podpis_dat));
}





void Balik::odesliHlavicku(){
    spojka->odesliHlavicku(balicek, mame_naklad);
}

void Balik::odesliNaklad(){
    spojka->odesliNaklad(naklad3);
}



bool Balik::odesli(bool uz_neprijimat){
    cout << bc.VIOLET << SERVER << "Zpráva bude odeslána" << bc.ENDC << endl;
    if(mame_naklad)
        this->podepisNaklad();
    this->podepisHlavicku();
    this->zasifrujHlavicku();
    if(mame_naklad)
        this->zasifrujNaklad();

    this->odesliHlavicku();
    this->odesliNaklad();

    cout << bc.VIOLET << SERVER << "Zpráva odeslána" << bc.ENDC << endl;

    if(!uz_neprijimat){
        int prijmuto = this->prijmi();
        printif(prijmuto, "Balicek Odeslan\n");
        if(prijmuto && balicek.hlavicka.ridic.typ == ackDat_e)
            cout << bc.GREEN << "Zpráva úspěšně odeslána" << endl;
        else
            cout << bc.RED << "Odeslání zprávy selhalo" << endl;
        if(!prijmuto)
            return false;
        if(balicek.hlavicka.ridic.typ == ackDat_e)
            return true;
    }

    return false;
}

int Balik::prijmi(){
    mame_naklad = 0;
    int uspech = 1;
    int ret = spojka->prijmi(balicek, naklad3);
    // printd("1 Prijmut typ %d\n", ret);

    if(ret == -1)
        return -1;

    if(ret == STREAM_DATA){
        mame_naklad = true;
        ret = spojka->prijmi(balicek, naklad3);
        // printd("2 Prijmut typ %d\n", ret);
    }
    cout << bc.VIOLET << SERVER << "Přijal zprávu" << bc.ENDC << endl;

    PAesPad("Recieved_RSA_AES_Padding=");
    PMD5Pad("Recieved_AES_MD5_aes-key_padding=", podpis_hlavicky);

    this->desifrujHlavicku();
    PAes("Recieved_");
    int podpis1 = this->overPodpisHlavcky();
    printif(podpis1, "Podpis Hlavicky");
    uspech &= podpis1;

    if(balicek.hlavicka.ridic.typ == poslaniDat_e){
        if(!mame_naklad){
            ret = spojka->prijmi(balicek, naklad3);
            // printd("3 Prijmut typ %d\n", ret);
            if(ret == STREAM_DATA)
                mame_naklad = true;
        }
    }
    if(mame_naklad){
        PMD5Pad("Recieved_AES_MD5_message_padding=", podpis_dat);

        cout << bc.YELLOW << SERVER << "ciphertext=" << bc.ENDC;
        for(auto a : naklad3){
            printf("%.2x", a);
        }
        cout << endl;

        this->desifrujNaklad();
        int podpis2 = this->overPodpisNakladu();
        printif(podpis2, "Podpis Nakladu");
        uspech &= podpis2;

        cout << bc.YELLOW << SERVER << "plaintext_hex=" << bc.ENDC;
        for(auto a : naklad3){
            printf("%.2x", a);
        }
        cout << endl;

        Balik odpoved(*this);
        odpoved.vlozAck(uspech);
        odpoved.odesli(true);

        printif(uspech, "Balicek Prijmut\n");
    }


    return uspech;
}



const char * typy_str[] = {
    "nepouzito",
    "ackDat",
    "NackDat",
    "poslaniDat",
    "podpisHlavicky",
    "podpisDat",
    "chyba"
};

void balikosPrintos(struct rsaBalik_L1 &balicek){
    printc(bc.YELLOW, "\thlavicka:\n");
    printc(bc.CYAN, "\t\tkolotoc: ");
    printd("0x%.8lx\n", balicek.kolotoc);
    printc(bc.CYAN, "\t\tridic:\n");
    printc(bc.CYAN, "\t\t\tvelikost aes nakladu: ");
    printd("%lu\n", balicek.ridic.velikost_aes_nakladu);

    int __typ = (balicek.ridic.typ < 6) ? (balicek.ridic.typ) : (6);
    printc(bc.CYAN, "\t\t\ttyp: ");
    printd("%d %s%s%s\n", __typ, bc.VIOLET, typy_str[__typ], bc.ENDC);

    if(__typ == poslaniDat_e){
        printc(bc.CYAN, "\t\taes klic: ");
        for(int i = 0; i < AES_KLIC_VEL; ++i)
            printd("%.2x ", balicek.aes_klic[i]);
        printd("\n");
        printc(bc.CYAN, "\t\taes iv:   ");
        for(int i = 0; i < AES_IV_VEL; ++i)
            printd("%.2x ", balicek.aes_iv[i]);
        printd("\n");
    }
    if(__typ == podpisHlavicky_e || __typ == podpisDat_e){
        printc(bc.CYAN, "\t\thash: ");
        for(int i = 0; i < HASH_SIZE; ++i)
            printd("%.2x ", balicek.podpis[i]);
        printd("\n");
    }
}


void Balik::print(){
    printc(bc.BLUE, "#################################################\n");
    printc(bc.GREEN, "Balik:\n");
    balikosPrintos(balicek.hlavicka);
    balikosPrintos(balicek.podpis_hlavicky);
    if(balicek.hlavicka.ridic.typ == poslaniDat_e){
        balikosPrintos(balicek.podpis_dat);
    }
    if(balicek.hlavicka.ridic.typ != poslaniDat_e)
        printd("\n");
    printc(bc.RED, "\tnaklad: ");
    for(uint8_t a : naklad3){
        printd("0x%.2x ", a);
    }
    printd("\n");
    if(balicek.hlavicka.ridic.typ == poslaniDat_e){
        printd("\t%snaklad: %s%s%s\n", bc.RED, bc.GREEN, naklad3.c_str(), bc.ENDC);
    }

    printc(bc.BLUE, "#################################################\n");
    printd("\n");
}




#define YOLOTOC(DATOVYTYP, KOLOVEL, UKOLO, UPOLE) \
do {\
    uint8_t bits = sizeof(DATOVYTYP) * 8;\
    for(int i = 0; i < KOLOVEL; ++i){\
        uint8_t xorujo = UKOLO[i % (64 / bits)];\
        uint8_t rotujo = xorujo % bits;\
        UPOLE[i] = (UPOLE[i] << rotujo) | (UPOLE[i] >> (bits - rotujo));\
        UPOLE[i] ^= rotujo;\
    }\
} while(0)

#define COLOTOY(DATOVYTYP, KOLOVEL, UKOLO, UPOLE) \
do {\
    uint8_t bits = sizeof(DATOVYTYP) * 8;\
    for(int i = 0; i < KOLOVEL; ++i){\
        uint8_t xorujo = UKOLO[i % (64 / bits)];\
        uint8_t rotujo = xorujo % bits;\
        UPOLE[i] ^= rotujo;\
        UPOLE[i] = (UPOLE[i] >> rotujo) | (UPOLE[i] << (bits - rotujo));\
    }\
} while(0)

// #define atrakce(OKOLIK, KOLOVEL, UPOLE)\
//     for(int i = 0, j = KOLOVEL; i < (KOLOVEL/2) && j >= 0; i += 1, j -= 1){\
//         uint32_t tmp = UPOLE[i];\
//         UPOLE[i] = UPOLE[j - OKOLIK];\
//         UPOLE[j - OKOLIK] = tmp;\
//     }
#define atrakce(OKOLIK, KOLOVEL, UPOLE)

void xolotoc(struct rsaBalik_L1 *data){
    struct rsaBalik_L0 d;
    d.L1 = *data;

    YOLOTOC(uint8_t, KOLOSIZE_1, d.kolotoc1, d.kolo1);
    atrakce(d.kolotoc1[3] % (KOLOSIZE_3 / 4), KOLOSIZE_3, d.kolo3);
    YOLOTOC(uint16_t, KOLOSIZE_2, d.kolotoc2, d.kolo2);
    atrakce(d.kolotoc1[2] % (KOLOSIZE_3 / 8), KOLOSIZE_3, d.kolo3);
    YOLOTOC(uint32_t, KOLOSIZE_3, d.kolotoc3, d.kolo3);
    atrakce(d.kolotoc1[1] % (KOLOSIZE_2 / 16), KOLOSIZE_2, d.kolo2);
    YOLOTOC(uint64_t, KOLOSIZE_4, d.kolotoc4, d.kolo4);
    atrakce(d.kolotoc1[0] % (KOLOSIZE_1 / 32), KOLOSIZE_1, d.kolo1);

    *data = d.L1;
}

void colotox(struct rsaBalik_L1 *data){
    struct rsaBalik_L0 d;
    d.L1 = *data;

    atrakce(d.kolotoc1[0] % (KOLOSIZE_1 / 32), KOLOSIZE_1, d.kolo1);
    COLOTOY(uint64_t, KOLOSIZE_4, d.kolotoc4, d.kolo4);
    atrakce(d.kolotoc1[1] % (KOLOSIZE_2 / 16), KOLOSIZE_2, d.kolo2);
    COLOTOY(uint32_t, KOLOSIZE_3, d.kolotoc3, d.kolo3);
    atrakce(d.kolotoc1[2] % (KOLOSIZE_3 / 8), KOLOSIZE_3, d.kolo3);
    COLOTOY(uint16_t, KOLOSIZE_2, d.kolotoc2, d.kolo2);
    atrakce(d.kolotoc1[3] % (KOLOSIZE_3 / 4), KOLOSIZE_3, d.kolo3);
    COLOTOY(uint8_t, KOLOSIZE_1, d.kolotoc1, d.kolo1);

    *data = d.L1;
}


void balikPrint(struct rsaBalik_L1 *data){
    printc(bc.GREEN, "rsaBalik_L1:\n");

    printc(bc.YELLOW, "\tkolotoc: ");
    printd("0x%.8lx\n", data->kolotoc);

    printc(bc.YELLOW, "\tridic: ");
    printd("0x%.2lx ", data->ridic.velikost_aes_nakladu);
    printd("0x%.2x ", data->ridic.id);
    printd("0x%.2x ", data->ridic.typ);
    printd("0x%.2x ", data->ridic.vypln_posledniho_aes_bloku);
    printd("\n");

    printc(bc.YELLOW, "\tnaklad: \n");
    for(int i = 0; i < 228; ++i)
        printd("%.2x ", data->naklad[i]);
    printd("\n");
}
