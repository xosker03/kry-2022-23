#ifndef BALIK_H
#define BALIK_H

#include <stdint.h>

#include <string>
#include <vector>

#include "rsus.h"
#include "saes.h"
#include "spojovac.h"





void xolotoc(struct rsaBalik_L1 *data);
void colotox(struct rsaBalik_L1 *data);
void balikPrint(struct rsaBalik_L1 *data);




class Balik
{
public:
    Balik(RSUS *_mojeRsa, RSUS *_ciziRsa, Spojovac *_spojka);
    Balik(const Balik& other);
    ~Balik();
    //Balik& operator=(const Balik& other);
    //bool operator==(const Balik& other) const;
    //bool operator!=(const Balik& other) const;

    void vlozAck(bool ack);
    void vlozNaklad(balikNaklad_t data);

    void podepisNaklad();
    void podepisHlavicku();
    void zasifrujNaklad();
    void zasifrujHlavicku();

    void desifrujHlavicku();
    bool overPodpisHlavcky();
    void desifrujNaklad();
    bool overPodpisNakladu();

    void odesliHlavicku();
    void odesliNaklad();

    bool odesli(bool uz_neprijimat = false);
    int prijmi();

    void print();

    struct rsaBalik_L2 balicek;
    balikNaklad_t naklad3;

private:
    SAES mujAes;
    RSUS *mojeRsa;
    RSUS *ciziRsa;
    Spojovac *spojka;

    aesKlic_t klic;
    aesIV_t iv;

    bool mame_naklad;
};

#endif // BALIK_H
