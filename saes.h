#ifndef SAES_H
#define SAES_H

#include <string>
#include <vector>

#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/files.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define AES_KLIC_VEL 16
#define AES_IV_VEL 16

using namespace std;
using namespace CryptoPP;

typedef string aesData_t;
typedef vector<uint8_t> aesKlic_t;
typedef vector<uint8_t> aesIV_t;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class SAES
{
public:
    SAES();
    ~SAES();
    void aesSet(aesKlic_t key, aesIV_t _iv = aesIV_t());

    aesData_t aesSifruj(string plaintext);
    string aesDesifruj(aesData_t ciphertext);
    vector<uint8_t> aesSifruj(vector<uint8_t> plaintext);
    vector<uint8_t> aesDesifruj(vector<uint8_t> ciphertext);

    aesKlic_t aesGenKlic();
    aesIV_t aesGenIV();
    void aesPrint(aesData_t data);
    void aesPrint(aesKlic_t data);

private:
    OFB_Mode< AES >::Encryption enc;
    OFB_Mode< AES >::Decryption dec;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // SAES_H
