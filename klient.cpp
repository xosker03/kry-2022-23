#include "klient.h"
#include "zaklad.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>

#include<unistd.h>


static void die(const char *s) {
        perror(s);
        exit(1);
}


Klient::Klient(int port, string ip) : Spojovac()
{
    int ret;
    const char *msg = "Hello, Server!";
    const char *msg2 = "Hello, Server! 2";
    const char *msg3 = "Hello, Server! 3";

    struct sockaddr_in servaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    struct sctp_initmsg initmsg = {
        .sinit_num_ostreams = 5,
        .sinit_max_instreams = 5,
        .sinit_max_attempts = 4,
    };

    conn_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);
    if (conn_fd < 0)
            die("socket()");

    ret = setsockopt(conn_fd, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");
    ret = setsockopt(conn_fd, IPPROTO_SCTP, SCTP_EVENTS, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");
    ret = setsockopt(conn_fd, IPPROTO_SCTP, SCTP_NODELAY, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");

    ret = connect(conn_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));
    if (ret < 0)
            die("connect()");

    // ret = sctp_sendmsg(conn_fd, (void *) msg, strlen(msg) + 1, NULL, 0, 0, 0, 0 /* stream */, 0, 0 );
    // if (ret < 0)
    //         die("sctp_sendmsg");
    // ret = sctp_sendmsg(conn_fd, (void *) msg2, strlen(msg2) + 1, NULL, 0, 0, 0, 1 /* stream */, 0, 0 );
    // if (ret < 0)
    //         die("sctp_sendmsg");
    // ret = sctp_sendmsg(conn_fd, (void *) msg3, strlen(msg3) + 1, NULL, 0, 0, 0, 1 /* stream */, 0, 0 );
    // if (ret < 0)
    //         die("sctp_sendmsg");


}

Klient::~Klient()
{
        sleep(1);
        close(conn_fd);
        sleep(1);
}


void Klient::odesliHlavicku(struct rsaBalik_L2 &hlavicka, bool mame_naklad){
        int velikost = sizeof(hlavicka);
        if(!mame_naklad)
                velikost -= SIZE_rsaBalik_L1;
        // printd("odesilam hlavicku %d\n", velikost);
        int ret = sctp_sendmsg(conn_fd, (void *) &hlavicka, velikost, NULL, 0, 0, 0, STREAM_HLAVICKA /* stream */, 0, 0 );
}

void Klient::odesliNaklad(balikNaklad_t &naklad){
        // printd("odesilam naklad %lu\n", naklad.size());
        int ret = sctp_sendmsg(conn_fd, (void *) naklad.c_str(), naklad.size(), NULL, 0, 0, 0, STREAM_DATA /* stream */, 0, 0 );
}

int Klient::prijmi(struct rsaBalik_L2 &hlavicka, balikNaklad_t &naklad){
        uint8_t buffer[1024];
        struct sctp_sndrcvinfo sndrcvinfo = {0};
        int flags = 0;
        int in = sctp_recvmsg(conn_fd, buffer, sizeof(buffer), NULL, 0, &sndrcvinfo, &flags);
        // printd("prijmuto bajtu %d\n", in);
        if(in == 0){
            printc(bc.RED,"Spojeni ztraceno asi\n");
            return -1;
        }

        if(sndrcvinfo.sinfo_stream == STREAM_HLAVICKA){
                hlavicka = *((rsaBalik_L2 *) buffer);
        }
        if(sndrcvinfo.sinfo_stream == STREAM_DATA){
                naklad.resize(0);
                for(int i = 0; i < in; ++i)
                        naklad.push_back(buffer[i]);
        }
        // printc(bc.VIOLET);
        // printd("sinfo_stream %u\n", sndrcvinfo.sinfo_stream);
        // printd("sinfo_ssn %u\n", sndrcvinfo.sinfo_ssn);
        // printd("sinfo_flags %u\n", sndrcvinfo.sinfo_flags);
        // printd("sinfo_ppid %u\n", sndrcvinfo.sinfo_ppid);
        // printd("sinfo_context %u\n", sndrcvinfo.sinfo_context);
        // printd("sinfo_timetolive %u\n", sndrcvinfo.sinfo_timetolive);
        // printd("sinfo_tsn %u\n", sndrcvinfo.sinfo_tsn);
        // printd("sinfo_cumtsn %u\n", sndrcvinfo.sinfo_cumtsn);
        // printd("sinfo_assoc_id %u\n", sndrcvinfo.sinfo_assoc_id);
        // printendc();

        if(sndrcvinfo.sinfo_stream == STREAM_IGNORE){
            return this->prijmi(hlavicka, naklad);
        }

        return sndrcvinfo.sinfo_stream;
}













