#include "server.h"
#include "zaklad.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>

#include<unistd.h>



static void die(const char *s) {
        perror(s);
        exit(1);
}


//https://gist.github.com/zonque/7d03568eab14a2bb57cb
//https://docs.oracle.com/cd/E19120-01/open.solaris/817-4415/sockets-30/index.html
//https://docs.oracle.com/cd/E19120-01/open.solaris/817-4415/sockets-329/index.html
//https://developers.redhat.com/blog/2018/01/03/sctp-stream-schedulers#
//https://www.uobabylon.edu.iq/eprints/publication_6_17163_1425.pdf


Server::Server(int port) : Spojovac()
{
     int listen_fd, flags, ret, in;
    struct sctp_sndrcvinfo sndrcvinfo = {0};

    struct sockaddr_in servaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    struct sctp_initmsg initmsg = {
            .sinit_num_ostreams = 5,
            .sinit_max_instreams = 5,
            .sinit_max_attempts = 4,
    };

    listen_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);
    if (listen_fd < 0)
            die("socket");

    ret = bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));
    if (ret < 0)
            die("bind");

    ret = setsockopt(listen_fd, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");
    ret = setsockopt(listen_fd, IPPROTO_SCTP, SCTP_EVENTS, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");
    ret = setsockopt(listen_fd, IPPROTO_SCTP, SCTP_NODELAY, &initmsg, sizeof(initmsg));
    if (ret < 0)
            die("setsockopt");

    ret = listen(listen_fd, initmsg.sinit_max_instreams);
    if (ret < 0)
            die("listen");

    for (;;) {
        char buffer[1024];

        printd("Waiting for connection\n");
        fflush(stdout);

        conn_fd = accept(listen_fd, (struct sockaddr *) NULL, NULL);
        if(conn_fd < 0)
                die("accept()");

        printd("New client connected\n");
        //fflush(stdout);

        // while(1){
        //     in = sctp_recvmsg(conn_fd, buffer, sizeof(buffer), NULL, 0, &sndrcvinfo, &flags);
        //     if (in > 0) {
        //         //printf("Received data: %s\n", buffer);
        //         printf("Přijata data ze streamu %u: %s\n", sndrcvinfo.sinfo_stream, buffer);
        //         printf("TSN %u, SSN %u\n\n", sndrcvinfo.sinfo_tsn, sndrcvinfo.sinfo_ssn);
        //         fflush(stdout);
        //     } else {
        //         break;
        //     }
        // }

        break;
    }
}

Server::~Server()
{
        sleep(1);
        close(conn_fd);
        sleep(1);
}


void Server::odesliHlavicku(struct rsaBalik_L2 &hlavicka, bool mame_naklad){
        int velikost = sizeof(hlavicka);
        if(!mame_naklad)
                velikost -= SIZE_rsaBalik_L1;
        // printd("odesilam hlavicku %d\n", velikost);
        int ret = sctp_sendmsg(conn_fd, (void *) &hlavicka, velikost, NULL, 0, 0, 0, STREAM_HLAVICKA /* stream */, 0, 0 );
}

void Server::odesliNaklad(balikNaklad_t &naklad){
        // printd("odesilam naklad %lu\n", naklad.size());
        int ret = sctp_sendmsg(conn_fd, (void *) naklad.c_str(), naklad.size(), NULL, 0, 0, 0, STREAM_DATA /* stream */, 0, 0 );
}

int Server::prijmi(struct rsaBalik_L2 &hlavicka, balikNaklad_t &naklad){
        uint8_t buffer[1024];
        struct sctp_sndrcvinfo sndrcvinfo = {0};
        int flags = 0;
        int in = sctp_recvmsg(conn_fd, buffer, sizeof(buffer), NULL, 0, &sndrcvinfo, &flags);
        // printd("prijmuto bajtu %d\n", in);
        if(in == 0){
            printc(bc.RED,"Spojeni ztraceno asi\n");
            return -1;
        }

        if(sndrcvinfo.sinfo_stream == STREAM_HLAVICKA){
                hlavicka = *((rsaBalik_L2 *) buffer);
        }
        if(sndrcvinfo.sinfo_stream == STREAM_DATA){
                naklad.resize(0);
                for(int i = 0; i < in; ++i)
                        naklad.push_back(buffer[i]);
        }
        // printc(bc.VIOLET);
        // printd("sinfo_stream %u\n", sndrcvinfo.sinfo_stream);
        // printd("sinfo_ssn %u\n", sndrcvinfo.sinfo_ssn);
        // printd("sinfo_flags %u\n", sndrcvinfo.sinfo_flags);
        // printd("sinfo_ppid %u\n", sndrcvinfo.sinfo_ppid);
        // printd("sinfo_context %u\n", sndrcvinfo.sinfo_context);
        // printd("sinfo_timetolive %u\n", sndrcvinfo.sinfo_timetolive);
        // printd("sinfo_tsn %u\n", sndrcvinfo.sinfo_tsn);
        // printd("sinfo_cumtsn %u\n", sndrcvinfo.sinfo_cumtsn);
        // printd("sinfo_assoc_id %u\n", sndrcvinfo.sinfo_assoc_id);
        // printendc();

        if(sndrcvinfo.sinfo_stream == STREAM_IGNORE){
            return this->prijmi(hlavicka, naklad);
        }

        return sndrcvinfo.sinfo_stream;
}

















