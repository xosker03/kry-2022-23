#include "zaklad.hpp"
#include <stdio.h>
#include <string>
#include <array>
#include <iostream>
#include <thread>

#ifndef NOCOLORS

struct bcolors bc = {
    .BLUE = "\033[94m",
    .GREEN = "\033[92m",
	.RED = "\033[91m",
	.VIOLET = "\033[95m",
	.YELLOW = "\033[93m",
	.CYAN = "\033[96m",
	.DBLUE = "\033[34m",
	.DGREEN = "\033[32m",
	.DRED = "\033[31m",
	.DVIOLET = "\033[35m",
	.DYELLOW = "\033[33m",
    .DCYAN = "\033[36m",
    .WHITE = "\033[97m",
    .DGRAY = "\033[90m",
    .GRAY = "\033[37m",
    .BLACK = "\033[30m",
	.BOLD = "\033[1m",
	.UNDERLINE = "\033[4m",
	.CRITIC = "\033[41;1m",
	.ENDC = "\033[0m",
	.CURSORH = "\033[?25l",
	.CURSORS = "\033[?25h",
	.chyba = "\033[91mChyba: \033[0m"
};

#else

struct bcolors bc = {
    .BLUE = "",
    .GREEN = "",
	.RED = "",
	.VIOLET = "",
	.YELLOW = "",
	.CYAN = "",
	.DBLUE = "",
	.DGREEN = "",
	.DRED = "",
	.DVIOLET = "",
	.DYELLOW = "",
    .DCYAN = "",
    .WHITE = "",
    .DGRAY = "",
    .GRAY = "",
    .BLACK = "",
	.BOLD = "",
	.UNDERLINE = "",
	.CRITIC = "",
	.ENDC = "",
	.CURSORH = "",
	.CURSORS = "",
	.chyba = "Chyba: "
};

#endif

bool printif(bool p){
    if(p)
        printd("%s", bc.GREEN);
    else
        printd("%s", bc.RED);
    return p;
}
bool printif(bool p, const char * data){
    if(p)
        printd("%s%s%s\n", bc.GREEN, data, bc.ENDC);
    else
        printd("%s%s%s\n", bc.RED, data, bc.ENDC);
    return p;
}
void printendc(){
    printd("%s", bc.ENDC);
}
void printc(const char * color){
    printd("%s", color);
}
void printc(const char * color, const char * data){
    printd("%s%s%s", color, data, bc.ENDC);
}
int voidfun(){
    return 0;
}





#ifndef ONLY_COLORS

std::string shell_pipe(const std::string cmd, int& out_exitStatus)
{
    out_exitStatus = 0;
    auto pPipe = ::popen(cmd.c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }

    std::array<char, 256> buffer;

    std::string result;

    while(not std::feof(pPipe)){
        auto bytes = std::fread(buffer.data(), 1, buffer.size(), pPipe);
        result.append(buffer.data(), bytes);
    }

    auto rc = ::pclose(pPipe);

    if(WIFEXITED(rc)){
        out_exitStatus = WEXITSTATUS(rc);
    }

    return result;
}


int shell_call(const std::string cmd){
	std::cout << bc.BLUE << cmd << bc.ENDC << std::endl;
    auto pPipe = ::popen((cmd).c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }
	char tmp[1024];
    while(not std::feof(pPipe)){
        std::fread(tmp, 1, 1024, pPipe);
    }
    int rc = ::pclose(pPipe);
	return rc;
}

void shell_throw(const std::string cmd){
	std::thread first (shell_call, cmd + " 2>&1");
	first.detach();
}



#endif
