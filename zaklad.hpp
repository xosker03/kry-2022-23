#ifndef __ZAKLAD_H__
#define __ZAKLAD_H__

#include <string>

#ifndef NODEBUG
	#define printd(...) fprintf(stderr, __VA_ARGS__)
#else
	#define printd(...) voidfun()
#endif


struct bcolors{
	const char *BLUE;
	const char *GREEN;
	const char *RED;
	const char *VIOLET;
	const char *YELLOW;
	const char *CYAN;
	const char *DBLUE;
	const char *DGREEN;
	const char *DRED;
	const char *DVIOLET;
	const char *DYELLOW;
	const char *DCYAN;
	const char *WHITE;
	const char *DGRAY;
	const char *GRAY;
	const char *BLACK;
	const char *BOLD;
	const char *UNDERLINE;
	const char *CRITIC;
	const char *ENDC;
	const char *CURSORH;
	const char *CURSORS;
	const char *chyba;
};

extern struct bcolors bc;


bool printif(bool p);
bool printif(bool p, const char * data);
void printendc();
void printc(const char * color);
void printc(const char * color, const char * data);
int voidfun();



std::string shell_pipe(const std::string cmd, int& out_exitStatus);
int shell_call(const std::string cmd);
void shell_throw(const std::string cmd);


#endif
