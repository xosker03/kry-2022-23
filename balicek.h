#ifndef __BALICEK__H_
#define __BALICEK__H_

#include <stdint.h>

#include <string>
#include <vector>

#include "saes.h"


#define KOLOSIZE_1 248
#define KOLOSIZE_2 124
#define KOLOSIZE_3 62
#define KOLOSIZE_4 31

#define SIZE_typZpravy 1
#define SIZE_ridic 16
#define SIZE_rsaBalik_L2 (SIZE_rsaBalik_L1*3)
#define SIZE_rsaBalik_L1 256
#define SIZE_rsaBalik_L0 SIZE_rsaBalik_L1

#define MAX_PAYLOAD (256 - sizeof(uint64_t) - SIZE_ridic) //232

#define HASH_SIZE 16



/*
 * Odesílatel:
 *      má data
 *      vytvoří balíček
 *      vloží do balíčku
 *      podepíše hlavičku a data
 *      zašifruje ho
 *      hlavičku pošle přeš stream1
 *      data pošle přes stream2
 *      čeká na odpověď na streamu1
 *      dostane - ack -> vyčistí balíček
 *              - nack -> pošle znovu
 *              - nic -> počká na timeout a pošle znovu
 *              špatný podpis/integrita - ack -> stejně jako nic
 *                                      - nack -> stejně jako nic
 *
 * Příjemce:
 *      čeká na hlavičku na streamu1
 *      přijme hlavičku
 *      dešifruje hlavičku
 *      oveří podpis hlavičky
 *      přijme n bytů dat podle hlavičky
 *      ověří podpis data
 *      pokud vše sedí pošle podepsaný ack
 */

//řídící struktury
enum typZpravy : uint8_t {
    nepouzito_e = 0,
    ackDat_e = 1,
    nackDat_e = 2,
    poslaniDat_e = 3,
    podpisHlavicky_e = 4,
    podpisDat_e = 5,
    //secondarySymKey_e = 4,
};

struct ridic {
    uint64_t velikost_aes_nakladu;
    uint32_t id;
    enum typZpravy typ;
    uint8_t vypln_posledniho_aes_bloku;
};

struct rsaBalik_L1 {
    uint64_t kolotoc;
    struct ridic ridic;
    union {
        uint8_t naklad[MAX_PAYLOAD];
        struct {
            uint8_t aes_klic[AES_KLIC_VEL];
            uint8_t aes_iv[AES_IV_VEL];
            uint8_t aes_naklad[MAX_PAYLOAD - AES_KLIC_VEL - AES_IV_VEL];
        };
        struct {
            uint8_t podpis[HASH_SIZE];
            uint8_t podpisova_vypln[MAX_PAYLOAD - HASH_SIZE];
        };

    };
};
//uint8_t vlastni_podpis[HASH_SIZE];

struct rsaBalik_L2 {
    struct rsaBalik_L1 hlavicka;
    struct rsaBalik_L1 podpis_hlavicky;
    struct rsaBalik_L1 podpis_dat;
    //uint8_t naklad2[];
};


typedef string balikNaklad_t;


#endif
