#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>


#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/files.h>

//#include <cryptopp/rsa.h>

#include "main.h"
#include "zaklad.hpp"
#include "saes.h"
#include "rsus.h"
#include "podpora.h"
#include "balik.h"
#include "spojovac.h"
#include "klient.h"
#include "server.h"

using namespace std;
using namespace CryptoPP;



string SERVER;
int MODE_SERVER;
int PORT = 1234;
string IPadresa;

void signal_callback_handler(int signum){
        printd("Caught signal SIGPIPE %d\n",signum);
        throw 1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


string tiskRsaKlice(string k){
    string ret = "";
    string myText;
    ifstream MyReadFile(k);
    while (getline (MyReadFile, myText)) {
        char buffer[128] = {0};
        for(uint8_t a : myText){
            sprintf(buffer, "%.2x", a);
            ret += string(buffer);
        }
    }
    MyReadFile.close();

    return ret;
}



int main (int argc, char** argv){
    std::ios_base::sync_with_stdio(true);
    printd("%sStart%s\n", bc.BLUE, bc.ENDC);

    //signal(SIGPIPE, SIG_IGN);
    signal(SIGPIPE, signal_callback_handler);

    int c;
	while((c = getopt(argc, argv, "csp:")) != -1){
		switch(c){
			case 'c':{
				MODE_SERVER = 1;
                SERVER = "c: ";
				break;
			}
			case 's':{
				MODE_SERVER = 2;
                SERVER = "s: ";
				break;
			}
			case 'p':{
				PORT = strtol(optarg, NULL, 10);
				break;
			}
		}
	}

	printd("Server %d\n", MODE_SERVER);
    printd("Port %d\n", PORT);


    FRAND = fopen("/dev/random", "r");


    cout << bc.YELLOW << SERVER << "start" << bc.ENDC << endl;


    if(MODE_SERVER)
        printc(bc.CRITIC, "/////////////////////////////////////////////////////////////////////////////////////////////\n");

    if(MODE_SERVER == 1){
        Klient K(PORT);
        RSUS ja(true, "klice/peer1_rsa");
        RSUS on(true, "klice/peer2_rsa");
        Balik bal(&ja, &on ,&K);

        cout << bc.GREEN << SERVER << "Úspešně připojeno k serveru" << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_public_key_sender=" << bc.BLUE << tiskRsaKlice("klice/peer1_rsa.pub") << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_private_key_sender=" << bc.BLUE << tiskRsaKlice("klice/peer1_rsa") << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_public_key_reciever=" << bc.BLUE << tiskRsaKlice("klice/peer2_rsa.pub") << bc.ENDC << endl;

        cout << bc.GREEN << "c: Zadej vstup: " << bc.ENDC;
         for (std::string line; std::getline(std::cin, line);) {
            //std::cout << line << std::endl;
            if(line == ""){
                cout << bc.GREEN << "c: Zadej vstup: " << bc.ENDC;
                continue;
            }
            cout << bc.YELLOW << SERVER << "plaintext_hex=" << bc.ENDC;
            for(auto a : line){
                printf("%.2x", a);
            }
            cout << endl;
            bal.vlozNaklad(line);
            bal.print();
            bal.odesli();
            cout << bc.GREEN << "c: Zadej vstup: " << bc.ENDC;
        }
        cout << endl;
    }
    if(MODE_SERVER == 2){
        Server S(PORT);
        RSUS ja(true, "klice/peer2_rsa");
        RSUS on(true, "klice/peer1_rsa");
        Balik bal(&ja, &on ,&S);

        cout << bc.GREEN << SERVER << "Klient se připojil" << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_public_key_sender=" << bc.BLUE << tiskRsaKlice("klice/peer2_rsa.pub") << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_private_key_sender=" << bc.BLUE << tiskRsaKlice("klice/peer2_rsa") << bc.ENDC << endl;
        cout << bc.YELLOW << SERVER << "RSA_public_key_reciever=" << bc.BLUE << tiskRsaKlice("klice/peer1_rsa.pub") << bc.ENDC << endl;

        while(1){
            try {
                int uspech = bal.prijmi();
                if(uspech == -1)
                    break;
                bal.print();
                //printf("s: plaintext=%s\n", bal.naklad3.c_str());
                cout << bc.GREEN << SERVER << "plaintext=" << bal.naklad3 << bc.ENDC << endl;
            } catch (...) {

            }
        }
    }
    if(MODE_SERVER)
        return 0;





    printc(bc.BLUE, "Random\n");

    for(int i = 0; i < 10; ++i){
        printd("%d ", gimmeRandom());
    }
    printd("\n");




    printc(bc.BLUE, "MD5\n");

    Weak::MD5 hash;
    std::cerr << "Name: " << hash.AlgorithmName() << std::endl;
    std::cerr << "Digest size: " << hash.DigestSize() << std::endl;
    std::cerr << "Block size: " << hash.BlockSize() << std::endl;


    md5hash_t tmp = md5Hash("Yoda said, Do or do not. There is no try.");
    for(uint8_t a : tmp){
        printd("%x ", a);
    }
    printd("\n");




    printc(bc.BLUE, "AES\n");
    printc(bc.BLUE);
    printd("AES default size: key %d, iv %d\n", CryptoPP::AES::DEFAULT_KEYLENGTH, CryptoPP::AES::BLOCKSIZE);
    printendc();

    SAES A;

    aesKlic_t klic = A.aesGenKlic();
    aesIV_t iv = A.aesGenIV();

    string otevreno = "Muzete polozit jenom jednu a opovim vam jenom tri.";
    aesData_t sifrovano;
    string desifrovano;

    A.aesSet(klic, iv);

    sifrovano = A.aesSifruj(otevreno);
    desifrovano = A.aesDesifruj(sifrovano);
    //A.aesRecover();
    //desifrovano = A.aesDesifruj(sifrovano);

    printc(bc.GREEN, otevreno.c_str());
    printd("\n");
    A.aesPrint(klic);
    A.aesPrint(sifrovano);
    printif(otevreno == desifrovano, desifrovano.c_str());


    printc(bc.BLUE, "RSA public -> private\n");

    RSUS ja(true, "klice/peer1_rsa");

    string rvstup = "tohleto je super tajna zprava";
    string rvystup;
    rvystup = ja.rsaDesifruj(ja.rsaSifruj(rvstup));
    printif(rvstup == rvystup, "RSA sifrovani public -> private");

    // string rdata = "tohleto je super tajna zprava";
    // ja.rsaSifruj(rdata.c_str(), rdata.size());
    // ja.rsaDesifruj(rdata.c_str(), rdata.size());
    // printif(rvstup == rdata, "RSA sifrovani data public -> private");


    printc(bc.BLUE, "RSA private -> public\n");

    rvystup = ja.rsaOveruj(ja.rsaPodepisuj(rvstup));
    printif(rvstup == rvystup, "RSA podepisovani public -> private");


    int ret = 0;
#define TEMPVEL 256
    printc(bc.BLUE, "RSA block sifrování\n");
    {
        vector<uint8_t> rbdata;
        vector<uint8_t> rbdata2;
        rbdata.resize(TEMPVEL);
        FOR(TEMPVEL, {
            rbdata[i] = gimmeRandom();
            printd("%.2x ", rbdata[i]);
        });
        rbdata[0] = 0;
        rbdata2 = rbdata;
        printd("\n");
        ja.rsaSifruj(rbdata.data(), TEMPVEL);
        FOR(TEMPVEL,printd("%.2x ", rbdata[i]);); printd("\n");
        ja.rsaDesifruj(rbdata.data(), TEMPVEL);
        bool verifikace = printif(rbdata2 == rbdata);
        FOR(TEMPVEL,printd("%.2x ", rbdata[i]);); printd("\n");
        ret += !verifikace;
    }




    printc(bc.BLUE, "RSA block podpis\n");
    {
        vector<uint8_t> rbdata;
        vector<uint8_t> rbdata2;
        rbdata.resize(TEMPVEL);
        FOR(TEMPVEL, {
            rbdata[i] = gimmeRandom();
            printd("%.2x ", rbdata[i]);
        });
        rbdata[0] = 0;
        rbdata2 = rbdata;
        printd("\n");
        ja.rsaPodepisuj(rbdata.data(), TEMPVEL);
        FOR(TEMPVEL,printd("%.2x ", rbdata[i]);); printd("\n");
        ja.rsaOveruj(rbdata.data(), TEMPVEL);
        bool podpis = printif(rbdata2 == rbdata);
        FOR(TEMPVEL,printd("%.2x ", rbdata[i]);); printd("\n");
        ret += !podpis;
    }

    if(ret)
        return ret;



    printc(bc.BLUE, "XOLOTOC\n");

    struct rsaBalik_L1 balik = {
        0x64,
        (struct ridic) {1024, 0x25, ackDat_e, 0x12},
        {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
    };
    //balik.kolotoc = 0xe89483d1ee58d27c;
    gimmeRandoms(&balik.kolotoc, sizeof(balik.kolotoc));
    //gimmeRandoms(&balik.naklad, MAX_PAYLOAD);
    //for(int i = 0; i < MAX_PAYLOAD; ++i)
    //    balik.naklad[i] = i;

    struct rsaBalik_L1 balik2 = balik;
    balikPrint(&balik);
    xolotoc(&balik);
    balikPrint(&balik);
    colotox(&balik);
    balikPrint(&balik);



    printc(bc.BLUE, "\n\nBALIK\n");

    Spojovac spojka;
    RSUS ja1(true, "klice/peer1_rsa");
    RSUS on1(true, "klice/peer2_rsa");
    Balik bal(&ja1, &on1 ,&spojka);

    bal.vlozNaklad("Super tajny naklad");
    bal.print();
    bal.podepisNaklad();
    bal.podepisHlavicku();
    bal.zasifrujHlavicku();
    bal.zasifrujNaklad();

    bal.print();

    RSUS ja2(true, "klice/peer2_rsa");
    RSUS on2(true, "klice/peer1_rsa");
    Balik bal2(&ja2, &on2, &spojka);
    bal2.balicek = bal.balicek;
    bal2.naklad3 = bal.naklad3;


    bal2.desifrujHlavicku();
    bool podpis1 = bal2.overPodpisHlavcky();
    printif(podpis1, "Podpis Hlavicky");
    bal2.desifrujNaklad();
    bool podpis2 = bal2.overPodpisNakladu();
    printif(podpis2, "Podpis Nakladu");
    bal2.print();

    //return !podpis1 + !podpis2;


    fclose(FRAND);
	return 0;
}
























