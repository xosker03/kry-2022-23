#ifndef RSUS_H
#define RSUS_H

#include <string>

#include "cryptopp/rsa.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define RSA_BLOCK_SIZE 256

using namespace std;


typedef string rsaData_t;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class RSUS
{
public:
    RSUS(bool _privatni, string filename);
    RSUS(const RSUS& other);
    ~RSUS();
    RSUS& operator=(const RSUS& other);

    rsaData_t rsaSifruj(string plaintext);
    string rsaDesifruj(rsaData_t ciphertext);

    void rsaSifruj(void *data, int size);
    void rsaDesifruj(void *data, int size);


    rsaData_t rsaPodepisuj(string plaintext);
    string rsaOveruj(rsaData_t ciphertext);

    void rsaPodepisuj(void *data, int size);
    void rsaOveruj(void *data, int size);

private:
    bool privatni;
    CryptoPP::RSA::PrivateKey rsaPrivate;
    CryptoPP::RSA::PublicKey rsaPublic;

    CryptoPP::RSA::PrivateKey rsaSignPrivate;
    CryptoPP::RSA::PublicKey rsaSignPublic;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // RSUS_H
