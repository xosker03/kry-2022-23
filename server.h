#ifndef SERVER_H
#define SERVER_H

#include "spojovac.h"


using namespace std;



class Server : public Spojovac
{
public:
    Server(int port);
    ~Server();

    void odesliHlavicku(struct rsaBalik_L2 &hlavicka, bool mame_naklad) override;
    void odesliNaklad(balikNaklad_t &naklad) override;

    int prijmi(struct rsaBalik_L2 &hlavicka, balikNaklad_t &naklad) override;

private:
    int conn_fd;
};

#endif // SERVER_H
