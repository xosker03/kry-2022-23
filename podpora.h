#ifndef __PODPORA_H_
#define __PODPORA_H_

#include <stdint.h>

#include <string>
#include <vector>
#include <iostream>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define FOR(x, ...) \
for(int i = 0; i < (x); ++i){\
    __VA_ARGS__ \
}


using namespace std;


typedef vector<uint8_t> md5hash_t;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


extern FILE * FRAND;

extern string SERVER;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


md5hash_t md5Hash(string msg);
md5hash_t md5Hash(void * data, int size);


inline uint8_t gimmeRandom(){
    int i = fgetc(FRAND);
    if(i == EOF)
        throw "Velky Spatny";
    return i;
}

inline void gimmeRandoms(void * target, int num){
    uint8_t * d = (uint8_t*) target;
    for(int k = 0; k < num; ++k){
        int i = fgetc(FRAND);
        if(i == EOF)
            throw "Velky Spatny";
        d[k] = i;
    }
}

inline uint64_t gimmeRandom64(){
    uint64_t ret = 0;
    for(int i = 0; i < 8; ++i){
        ret = ret | (gimmeRandom() << i * 8);
    }
    return ret;
}



#endif
