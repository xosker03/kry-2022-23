#include "saes.h"
#include "zaklad.hpp"
#include "main.h"
#include "podpora.h"

static_assert(AES_IV_VEL == CryptoPP::AES::BLOCKSIZE, "CHYBA: nesedí velikost inicializačního vektoru");

SAES::SAES()
{

}

SAES::~SAES()
{

}

void SAES::aesSet(aesKlic_t key, aesIV_t _iv){
    CryptoPP::byte iv[ CryptoPP::AES::BLOCKSIZE ];

    if(_iv.size() == CryptoPP::AES::BLOCKSIZE)
        memcpy( iv, _iv.data(), CryptoPP::AES::BLOCKSIZE );
    else
        //throw "Neco tu smrdi";
        memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

    enc.SetKeyWithIV(key.data(), key.size(), iv, CryptoPP::AES::BLOCKSIZE);
    dec.SetKeyWithIV(key.data(), key.size(), iv, CryptoPP::AES::BLOCKSIZE);
}


//https://www.cryptopp.com/wiki/Advanced_Encryption_Standard
aesData_t SAES::aesSifruj(string plaintext){
    std::string ciphertext;
    StringSource s(plaintext, true,
        new StreamTransformationFilter(enc,
            new StringSink(ciphertext)
        ) // StreamTransformationFilter
    ); // StringSource
    return ciphertext;
}


string SAES::aesDesifruj(aesData_t ciphertext){
    std::string decryptedtext;
    StringSource s(ciphertext, true,
        new StreamTransformationFilter(dec,
            new StringSink(decryptedtext)
        ) // StreamTransformationFilter
    ); // StringSource
    return decryptedtext;
}


vector<uint8_t> SAES::aesSifruj(vector<uint8_t> plaintext){
    vector<uint8_t> ciphertext;
    VectorSource s(plaintext, true,
        new StreamTransformationFilter(enc,
            new VectorSink(ciphertext)
        ) // StreamTransformationFilter
    ); // StringSource
    return ciphertext;
}


vector<uint8_t> SAES::aesDesifruj(vector<uint8_t> ciphertext){
    vector<uint8_t> decryptedtext;
    VectorSource s(ciphertext, true,
        new StreamTransformationFilter(dec,
            new VectorSink(decryptedtext)
        ) // StreamTransformationFilter
    ); // StringSource
    return decryptedtext;
}


aesKlic_t SAES::aesGenKlic(){
    aesKlic_t a;
    int x = AES_KLIC_VEL;
    while(x \
             \
              \
               \
                \
                 --> 0)
        a.push_back(gimmeRandom());
    // cout << bc.YELLOW << SERVER << "AES_key=" << bc.CYAN;
    // for(auto k : a){
    //     printf("%.2x", k);
    // }
    // cout << bc.ENDC << endl;
    return a;
}


aesIV_t SAES::aesGenIV(){
    aesIV_t a;
    for(int i = 0; i < CryptoPP::AES::BLOCKSIZE; ++i)
        a.push_back(gimmeRandom());
    // cout << bc.YELLOW << SERVER << "AES_iv=" << bc.DCYAN;
    // for(auto k : a){
    //     printf("%.2x", k);
    // }
    // cout << bc.ENDC << endl;
    return a;
}


void SAES::aesPrint(aesData_t data){
    printc(bc.CYAN);
    for(uint8_t a : data){
        printd("%x ", a);
    }
    printd("\n");
    printendc();
}

void SAES::aesPrint(aesKlic_t data){
    printc(bc.VIOLET);
    for(uint8_t a : data){
        printd("%x ", a);
    }
    printd("\n");
    printendc();
}
