#include "rsus.h"

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <string>
using std::string;

#include <stdexcept>
using std::runtime_error;

#include <cryptopp/queue.h>
using CryptoPP::ByteQueue;

#include <cryptopp/files.h>
using CryptoPP::FileSource;
using CryptoPP::FileSink;

#include "cryptopp/dsa.h"
using CryptoPP::DSA;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

#include <cryptopp/cryptlib.h>
using CryptoPP::PrivateKey;
using CryptoPP::PublicKey;
using CryptoPP::BufferedTransformation;

#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;


#include "cryptopp/integer.h"
using CryptoPP::Integer;

#include "zaklad.hpp"

static void EncodePrivateKey(const string& filename, const RSA::PrivateKey& key);
static void EncodePublicKey(const string& filename, const RSA::PublicKey& key);
static void Encode(const string& filename, const BufferedTransformation& bt);

static void DecodePrivateKey(const string& filename, RSA::PrivateKey& key);
static void DecodePublicKey(const string& filename, RSA::PublicKey& key);
static void Decode(const string& filename, BufferedTransformation& bt);



RSUS::RSUS(bool _privatni, string filename)
{
    AutoSeededRandomPool rnd;
    privatni = _privatni;
    if(privatni){
        DecodePrivateKey(filename, rsaPrivate);
        if(!rsaPrivate.Validate(rnd, 3))
			throw runtime_error("Rsa private key validation failed");
        DecodePublicKey(filename + ".pub", rsaPublic);
        if(!rsaPublic.Validate(rnd, 3))
			throw runtime_error("Rsa public key validation failed");

        auto n = rsaPrivate.GetModulus();
        auto d = rsaPrivate.GetPrivateExponent();
        auto e = rsaPrivate.GetPublicExponent();
        rsaSignPrivate.Initialize(n, d, e);
        rsaSignPublic.Initialize(n, d);
    } else {
		DecodePublicKey(filename + ".pub", rsaPublic);
        if(!rsaPublic.Validate(rnd, 3))
			throw runtime_error("Rsa public key validation failed");

        auto n = rsaPublic.GetModulus();
        Integer d("0x11");
        auto e = rsaPublic.GetPublicExponent();
        rsaSignPrivate.Initialize(n, d, e);
        rsaSignPublic.Initialize(n, d);
    }
}

RSUS::RSUS(const RSUS& other)
{
    privatni = other.privatni;
    if(privatni)
        rsaPrivate = other.rsaPrivate;
    else
        rsaPublic = other.rsaPublic;
}

RSUS::~RSUS()
{

}

RSUS& RSUS::operator=(const RSUS& other)
{
    return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rsaData_t RSUS::rsaSifruj(string plaintext){
    Integer m, c;
	m = Integer((const CryptoPP::byte *)plaintext.data(), plaintext.size());
	// Encrypt
	c = rsaPublic.ApplyFunction(m);

    rsaData_t ret;
    size_t req = c.MinEncodedSize();
	ret.resize(req);
    c.Encode((CryptoPP::byte *)ret.data(), ret.size());

    return ret;
}

string RSUS::rsaDesifruj(rsaData_t ciphertext){
    AutoSeededRandomPool prng;
    string recovered;
    Integer c, r;

    c = Integer((const CryptoPP::byte *)ciphertext.data(), ciphertext.size());

    // Decrypt
	r = rsaPrivate.CalculateInverse(prng, c);

	size_t req = r.MinEncodedSize();
	recovered.resize(req);
	r.Encode((CryptoPP::byte *)recovered.data(), recovered.size());

    //cout << ciphertext.size() << " = " << recovered.size() << endl;
    return recovered;
}






void RSUS::rsaSifruj(void *data, int size){
    if(size != RSA_BLOCK_SIZE){
        printc(bc.CRITIC, "RSA: spatna velikost bloku\n");
        throw "xxx";
    }

    Integer m, c;
	m = Integer((const CryptoPP::byte *)data, size);

	// Encrypt
	c = rsaPublic.ApplyFunction(m);
    c.Encode((CryptoPP::byte *)data, size);
}

void RSUS::rsaDesifruj(void *data, int size){
    if(size != RSA_BLOCK_SIZE){
        printc(bc.CRITIC, "RSA: spatna velikost bloku\n");
        throw "xxx";
    }

    AutoSeededRandomPool prng;
    string recovered;
    Integer c, r;

    c = Integer((const CryptoPP::byte *)data, size);

    // Decrypt
	r = rsaPrivate.CalculateInverse(prng, c);
	r.Encode((CryptoPP::byte *)data, size);
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



rsaData_t RSUS::rsaPodepisuj(string plaintext){
     //string message;
    Integer m, c;

	//cout << "message: " << plaintext << endl;

	// Treat the message as a big endian array
	m = Integer((const CryptoPP::byte *)plaintext.data(), plaintext.size());
	//cout << "m: " << hex << m << endl;

	// Encrypt
	c = rsaSignPublic.ApplyFunction(m);
	//cout << "c: " << hex << c << endl;

    rsaData_t ret;
    size_t req = c.MinEncodedSize();
	ret.resize(req);
    c.Encode((CryptoPP::byte *)ret.data(), ret.size());

    return ret;
}

string RSUS::rsaOveruj(rsaData_t ciphertext){
    AutoSeededRandomPool prng;
    string recovered;
    Integer c, r;

    c = Integer((const CryptoPP::byte *)ciphertext.data(), ciphertext.size());
    //cout << "c: " << hex << c << endl;

    // Decrypt
	r = rsaSignPrivate.CalculateInverse(prng, c);
	//cout << "r: " << hex << r << endl;

	// Round trip the message
	size_t req = r.MinEncodedSize();
	recovered.resize(req);
	r.Encode((CryptoPP::byte *)recovered.data(), recovered.size());

	//cout << "recovered: " << recovered << endl;

    return recovered;
}


void RSUS::rsaPodepisuj(void *data, int size){
   if(size != RSA_BLOCK_SIZE){
        printc(bc.CRITIC, "RSA: spatna velikost bloku\n");
        throw "xxx";
    }

    Integer m, c;
	m = Integer((const CryptoPP::byte *)data, size);

	// Encrypt
	c = rsaSignPublic.ApplyFunction(m);
    c.Encode((CryptoPP::byte *)data, size);
}

void RSUS::rsaOveruj(void *data, int size){
     if(size != RSA_BLOCK_SIZE){
        printc(bc.CRITIC, "RSA: spatna velikost bloku\n");
        throw "xxx";
    }

    AutoSeededRandomPool prng;
    string recovered;
    Integer c, r;

    c = Integer((const CryptoPP::byte *)data, size);

    // Decrypt
	r = rsaSignPrivate.CalculateInverse(prng, c);
	r.Encode((CryptoPP::byte *)data, size);
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




static void EncodePrivateKey(const string& filename, const RSA::PrivateKey& key)
{
	// http://www.cryptopp.com/docs/ref/class_byte_queue.html
	ByteQueue queue;
	key.DEREncodePrivateKey(queue);

	Encode(filename, queue);
}

static void EncodePublicKey(const string& filename, const RSA::PublicKey& key)
{
	// http://www.cryptopp.com/docs/ref/class_byte_queue.html
	ByteQueue queue;
	key.DEREncodePublicKey(queue);

	Encode(filename, queue);
}

static void Encode(const string& filename, const BufferedTransformation& bt)
{
	// http://www.cryptopp.com/docs/ref/class_file_sink.html
	FileSink file(filename.c_str());

	bt.CopyTo(file);
	file.MessageEnd();
}

static void DecodePrivateKey(const string& filename, RSA::PrivateKey& key)
{
	// http://www.cryptopp.com/docs/ref/class_byte_queue.html
	ByteQueue queue;

	Decode(filename, queue);
	key.BERDecodePrivateKey(queue, false /*optParams*/, queue.MaxRetrievable());
}

static void DecodePublicKey(const string& filename, RSA::PublicKey& key)
{
	// http://www.cryptopp.com/docs/ref/class_byte_queue.html
	ByteQueue queue;

	Decode(filename, queue);
	key.BERDecodePublicKey(queue, false /*optParams*/, queue.MaxRetrievable());
}

static void Decode(const string& filename, BufferedTransformation& bt)
{
	// http://www.cryptopp.com/docs/ref/class_file_source.html
	FileSource file(filename.c_str(), true /*pumpAll*/);

	file.TransferTo(bt);
	bt.MessageEnd();
}


